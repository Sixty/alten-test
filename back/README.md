## BDD

Installer un serveur MySQL afin de créer la base "altenbaris".

User : root

Password : Baris1401



Pour créer la database ainsi que la table, voir le fichier `Script.sql` (vos paramètres permettont peut-être d'auto créer la table au lancement du back-end)

Ce fichier contient aussi la liste des produits du fichier products.json sous forme de requête INSERT.



## Back-End

Réalisé en Java Spring Boot, j'utilise les toutes dernières versions de Java et Spring Boot nécessitant un JDK 17 ainsi que Maven.


API de type REST.

Port d'écoute : 8065

Post de restriction pour utilisation du back (CrossOrigin) : 4200

Penser à ajouter le JDK 17 dans la structure du projet.

HTTPS non activé.

Dossiers `node_modules` et `.angular` supprimés pour gagner de l'espace disque.


### Dépendences

Si les dépendances ne sont pas installées, utiliser la requête `mvn clean install` afin de le faire.


### Configuration IntelliJ Community

Détail de la configuration :

- type : `Application`
- classe principale : `com.example.springboot.AltenApplication`
- arguments : `clean spring-boot:run -Dmaven.test.skip=true` <!-- les tests sont ignorés par manque de temps -->
- repertoire de travail : `.\back`


Le fichier de configuration est partagé au besoin : `\runConfigurations\RUN.xml`.

Fichier à placer à cet emplacement pour une utilisation simplifiée : `[PROJECT_LOCATION]\back\.idea\runConfigurations`.


### Postman

La configuration Postman est partagée afin de réaliser les tests : `Alten.postman_collection.json`.


### Fonctionnalités Front

Les différentes fonctionnalités permettant d'administrer les produits côté Front ont été connectées aux services Back.

Ajout d'une gestion asynchrone pour la méthode de création de produit.

Cela permet d'attendre le retour de la création avant d'ajouter le produit côté Front.

Cette gestion est nécessaire afin de bien récupérer l'identifiant créé automatiquement côté Back pour ce produit.
