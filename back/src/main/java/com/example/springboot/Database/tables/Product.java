package com.example.springboot.Database.tables;


import jakarta.persistence.*;
import org.json.JSONObject;

import java.util.Objects;

@Table(name = "products")
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "image")
    private String image;

    @Column(name = "price")
    private double price;

    @Column(name = "category")
    private String category;

    @Column(name = "number")
    private Integer number;

    @Column(name = "inventory_status")
    private String inventoryStatus;

    @Column(name = "rating")
    private Integer rating;

    // ***************** //
    // GETTERS & SETTERS //
    // ***************** //

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getInventoryStatus() {
        return inventoryStatus;
    }

    public void setInventoryStatus(String inventoryStatus) {
        this.inventoryStatus = inventoryStatus;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    private boolean isValidData(JSONObject o, String key) {
        return o.has(key) &&
                !Objects.equals(o.get(key), "") &&
                o.get(key) != null;
    }

    public Product setData (JSONObject o) {
        Product product = new Product();

        if (isValidData(o, "code"))             this.setCode(o.getString("code"));
        if (isValidData(o, "name"))             this.setName(o.getString("name"));
        if (isValidData(o, "description"))      this.setDescription(o.getString("description"));
        if (isValidData(o, "price"))            this.setPrice(o.getDouble("price"));
        if (isValidData(o, "quantity"))         this.setNumber(o.getInt("quantity"));
        if (isValidData(o, "inventoryStatus"))  this.setInventoryStatus(o.getString("inventoryStatus"));
        if (isValidData(o, "category"))         this.setCategory(o.getString("category"));
        if (isValidData(o, "image"))            this.setImage(o.getString("image"));
        if (isValidData(o, "rating"))           this.setRating(o.getInt("rating"));

        return product;
    }
}
