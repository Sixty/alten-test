package com.example.springboot.Database.Repository;

import com.example.springboot.Database.tables.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    //
}