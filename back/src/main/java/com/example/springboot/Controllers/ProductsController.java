package com.example.springboot.Controllers;

import com.example.springboot.Database.Repository.ProductRepository;
import com.example.springboot.Database.tables.Product;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static com.example.springboot.Global.Consts.localhost_url;

@RestController
public class ProductsController {
    @Autowired
    private ProductRepository productRepository;

    /********************/
    /******        ******/
    /******  GET   ******/
    /******        ******/
    /********************/

    @CrossOrigin(origins = { localhost_url })
    @GetMapping("/products")
    public ResponseEntity<String> getProducts() {
        try {
            List<Product> products = productRepository.findAll();

            JSONObject obj = new JSONObject();
            obj.put("products", products);

            return ResponseEntity.ok(obj.toString());
        } catch (Exception e) {
            String msg = "Erreur lors de la récupération des produits : " + e;
            return ResponseEntity.internalServerError().body(msg);
        }
    }

    @CrossOrigin(origins = { localhost_url })
    @GetMapping("/products/{id}")
    public ResponseEntity<String> getProduct(@PathVariable Integer id) {
        try {
            Optional<Product> optional = productRepository.findById(id);
            if (optional.isEmpty()) return ResponseEntity.notFound().build();

            Product product = optional.get();
            JSONObject obj = new JSONObject();
            obj.put("product", new JSONObject(product));

            return ResponseEntity.ok(obj.toString());
        } catch (Exception e) {
            String msg = "Erreur lors de la récupération du produit " + id + " : " + e;
            return ResponseEntity.internalServerError().body(msg);
        }
    }

    /*********************/
    /******         ******/
    /******  POST   ******/
    /******         ******/
    /*********************/

    @CrossOrigin(origins = { localhost_url })
    @PostMapping("/products")
    public ResponseEntity<String> addProduct(@RequestBody String str) {
        try {
            JSONObject json = new JSONObject(str);

            // Write datas into Product and save it
            Product product = new Product();
            product.setData(json);
            productRepository.save(product);

            // Return the new product with auto generated ID
            json = new JSONObject();
            json.put("product", new JSONObject(product));

            return ResponseEntity.status(HttpStatus.CREATED).body(json.toString());
        } catch (Exception e) {
            String msg = "Erreur lors de l'ajout du produit : " + e;
            return ResponseEntity.internalServerError().body(msg);
        }
    }

    /*********************/
    /******         ******/
    /******   PUT   ******/
    /******         ******/
    /*********************/

    @CrossOrigin(origins = { localhost_url })
    @PutMapping("/products")
    public ResponseEntity<String> updateProduct(@RequestBody String str) {
        try {
            JSONObject json = new JSONObject(str);

            if (!json.has("id")) throw new Exception("Merci de renseigner le champ 'ID'");
            Integer id = json.getInt("id");

            // Error if ID does not exist
            Optional<Product> optional = productRepository.findById(id);
            if (optional.isEmpty()) return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Le produit " + id + " n'existe pas !");

            // Update datas into Product and save it
            Product product = optional.get();
            product.setData(json);
            productRepository.save(product);

            return ResponseEntity.ok(null);
        } catch (Exception e) {
            String msg = "Erreur lors de la mise à jour du produit : " + e;
            return ResponseEntity.internalServerError().body(msg);
        }
    }

    /***********************/
    /******           ******/
    /******  DELETE   ******/
    /******           ******/
    /***********************/

    @CrossOrigin(origins = { localhost_url })
    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable Integer id) {
        try {
            Optional<Product> optional = productRepository.findById(id);
            if (optional.isEmpty()) return ResponseEntity.notFound().build();

            productRepository.deleteById(id);

            return ResponseEntity.ok(null);
        } catch (Exception e) {
            String msg = "Erreur lors de la suppression du produit " + id + " : " + e;
            return ResponseEntity.internalServerError().body(msg);
        }
    }
}