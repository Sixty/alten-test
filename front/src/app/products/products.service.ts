import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, lastValueFrom } from 'rxjs';
import { Product } from './product.class';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
    private BACK_URL: string = "http://localhost:8065/products";
    private static productslist: Product[] = null;
    private products$: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);

    constructor(private http: HttpClient) { }

    getProducts(): Observable<Product[]> {
        if(!ProductsService.productslist )
        {
            this.http.get<any>(this.BACK_URL).subscribe(data => {
                ProductsService.productslist = data.products;
                this.products$.next(ProductsService.productslist);
            });
        }
        else
        {
            this.products$.next(ProductsService.productslist);
        }

        return this.products$;
    }

    async create(prod: Product): Promise<Observable<Product[]>> {
        if (!!prod) {
            const promiseData$ = this.http.post<any>(this.BACK_URL, prod);
            const data = await lastValueFrom(promiseData$);

            ProductsService.productslist.push(data.product);
            this.products$.next(ProductsService.productslist);
        }

        return this.products$;
    }

    update(prod: Product): Observable<Product[]>{
        if (!!prod) {
            this.http.put(this.BACK_URL, JSON.parse(JSON.stringify(prod))).subscribe(() => {
                ProductsService.productslist.forEach(element => {
                    if(element.id == prod.id)
                    {
                        element.name = prod.name;
                        element.category = prod.category;
                        element.code = prod.code;
                        element.description = prod.description;
                        element.image = prod.image;
                        element.inventoryStatus = prod.inventoryStatus;
                        element.price = prod.price;
                        element.quantity = prod.quantity;
                        element.rating = prod.rating;
                    }
                });
                this.products$.next(ProductsService.productslist);
            });
        }

        return this.products$;
    }


    delete(id: number): Observable<Product[]>{
        if (!!id) {
            this.http.delete<any>('http://localhost:8065/products/' + id).subscribe(() => {
                ProductsService.productslist = ProductsService.productslist.filter(value => { return value.id !== id } );
                this.products$.next(ProductsService.productslist);
            });
        }

        return this.products$;
    }
}